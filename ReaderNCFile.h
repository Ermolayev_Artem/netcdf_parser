#pragma once

#include <fstream>
#include <cstdio>
#include <map>
#include <utility>
#include <functional>
#include <unordered_map>
#include "ReportCodes.h"
#include "nc_struct.h"

//using get_var_val_f = std::function<bool(variable *)>;
class reader_nc_file;

class reader_conf_nc_file
{
public:
	std::vector <std::string> name_conf_nc_list;
private:
    std::fstream ifs;
	std::string name_conf_nc;
	bool is_one = false; // ���� �������� �� ��������������� ��� ����������� ������ nc ��� �������.
public:
	bool read_file();
    bool get_is_nc_flag() const { return is_one; }
	reader_conf_nc_file(const std::string file_name):name_conf_nc(file_name) 
	{
		if (name_conf_nc.substr(name_conf_nc.find_last_of('.') + 1, std::string::npos) == "nc")
			is_one = true;
	}
	~reader_conf_nc_file() { ifs.close(); }
};

class reader_nc_file
{
public:
	//���������� ������������ � ���� ������ �������� � ���� ���������� ����������
	std::vector<attribute> attribute_list;
	std::vector<variable>  variable_list;
	std::vector<dimension> dimension_list;
    std::vector<variable*> variable_conf;
	int descr_f; // ���������� ��������� �����
private:

    std::map<int/*code_type*/, std::function<bool(reader_nc_file*, variable *)>> processing_type;

	std::string ncfile_name;
	std::multimap<ATTR_CODE/*parameter*/, std::string /*value*/> list_config;
	int dimensions_count = 0; // ������� ���������
	int variables_count = 0; // ������� ����������
	int attributes_count = 0; // ������� ���������
	int unlim_dimension_count = 0; // ������� �������������� ���������
public:
	reader_nc_file(std::multimap<ATTR_CODE/*parameter*/, std::string /*value*/> *list_config_t, std::string file_name):list_config(*list_config_t),
		ncfile_name(file_name)	{}
	bool read_all_data_file();
    void init_reader();

    template <class T>
    bool get_var_val(variable *var);
    template <class T>
    static bool shell_get_var_func(reader_nc_file*, variable* var);

	~reader_nc_file();
private:
    void read_dim_info();
    bool read_attr_info();
    void read_var_info();
	int	get_att_val(int attid, attribute *att);

    void __literal_comparison(std::string reference, std::string taste);
	bool __read_info();
	bool read_data_specified_in_config();
    //void initialize_lists();
	bool count_parameters();

};

