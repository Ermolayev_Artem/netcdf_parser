#include "DataInitJS.h"
#include <QList>

WriterF::WriterF(QObject *parent) : QObject(parent)
{

}

WriterF::WriterF(std::string str, std::vector<DataInitJS*>& mass_stor) : WriterF::WriterF()
{
    this->name = str + "_res";
    this->mass_storage = mass_stor;
}

void WriterF::file()
{
    std::ofstream ifs(name);
    if (!ifs.is_open()) report.send_report(ERROR_OPEN_FILE, 1, "Write results to file.");
    for(auto &i: mass_storage)
    {
        ifs << i->get_varname() << "\t";
    }
    ifs << "\n";
    size_t size = max_lenght();
    for(size_t i=0; i<size; i++)
    {
        for(auto &it: mass_storage)
        {
            if (it->get_lenght() <= i){ ifs << "\t"; continue;}

            ifs << it->get_val(i) << "\t";
        }
        ifs << "\n";
    }
    ifs.close();

}

//void WriterF::table(std::vector<DataInitJS> mass_storage, std::string name)
//{

//}

//void WriterF::console(std::vector<DataInitJS> mass_storage)
//{

//}

size_t WriterF::max_lenght()
{
    size_t max = 0;
    for(auto i: mass_storage)
    {
        if(i->get_lenght() > max)
        {
            max = i->get_lenght();
        }
    }
    return max;
}

DataInitJS::DataInitJS(QObject *parent) : QObject(parent)
{

}

DataInitJS::DataInitJS(variable *var_t, bool ch) : DataInitJS::DataInitJS()
{
    this->changeable = ch;
    this->var = var_t;
    this->name = var_t->name;
    this->max_dims = new std::vector<int>(var_t->get_val_dim());
}

DataInitJS::DataInitJS(std::string str, bool ch) : DataInitJS::DataInitJS()
{
    this->changeable = ch;
    this->name = str;
}

DataInitJS::~DataInitJS()
{
    delete this->max_dims;
}

double DataInitJS::getElem(std::initializer_list<int> list)
{
    return getElem(list);

}

std::string DataInitJS::get_varname() const
{
       return name;
}

size_t DataInitJS::get_lenght() const
{
    return output_val.size();
}
double DataInitJS::get_val(size_t num) const
{
    return output_val.at(num);
}
double DataInitJS::getElem(std::vector<int> list)
{
    //check it
    if (changeable)
    {
        if (list.size() > 1)
        {
            report.send_report(ERROR_OUT_OF_RANGE, 5,
                               "Accessing an array element",
                               var->name.c_str(),
                               ". Must use only one dimension.");
            return 0;
        }
        else
        {
            return output_val.at(list.at(0));
        }
    }
    if (list.size() != max_dims->size())
    {
        if(list.size() > max_dims->size())
        {
            report.send_report(ERROR_OUT_OF_RANGE, 5,
                               "Accessing an array element",
                               var->name.c_str(),
                               "Must use",
                               std::to_string(max_dims->size()).c_str(),
                               "dimensions.");
            return 0;
        }
        else if (list.size() < max_dims->size()) {
            int j = list.size();
            while ( j < max_dims->size() )
            {
                list.push_back(0);
            }
        }
    }
    for(int i=0; i<list.size(); i++)
    {
        if (list.at(i) > max_dims->at(i))
        {
            report.send_report(ERROR_OUT_OF_RANGE, 5,
                               "Accessing an array element",
                               var->name.c_str(),
                               ". Dimension pos",
                               std::to_string(list.at(i)).c_str(),
                               ", must be less than",
                               std::to_string(max_dims->at(i)).c_str());
            return 0;
        }
    }
    int acc = 0;
    std::vector<int> temp_vec(list);
    for(int i=temp_vec.size()-1;i>=0;i--)
    {
        acc += temp_vec[i]*helperFunc(i);
    }
    switch (var->type)
    {
    case NC_BYTE:
        return (double)static_cast<char*>(var->array)[acc];
    case NC_CHAR:
        return (double)static_cast<int8_t*>(var->array)[acc];
    case NC_SHORT:
        return (double)static_cast<int16_t*>(var->array)[acc];
    case NC_INT:
        return (double)static_cast<int*>(var->array)[acc];
    case NC_FLOAT:
        return (double)static_cast<float*>(var->array)[acc];
    case NC_DOUBLE:
        return (double)static_cast<double*>(var->array)[acc];
    case NC_UBYTE:
        return (double)static_cast<uint8_t*>(var->array)[acc];
    case NC_USHORT:
        return (double)static_cast<uint16_t*>(var->array)[acc];
    case NC_UINT:
        return (double)static_cast<uint32_t*>(var->array)[acc];
    case NC_INT64:
        return (double)static_cast<int64_t*>(var->array)[acc];
    case NC_UINT64:
        return (double)static_cast<uint64_t*>(var->array)[acc];
    default:

        return 0;
    }
}

double DataInitJS::get(QList<int> lst)
{
    return getElem(lst.toVector().toStdVector());
}

void DataInitJS::set(double val)
{
    if (changeable)
    {
        output_val.push_back(val);
    }
    else
    {
        report.send_report(ERROR_SOURCE_DATA, 3,
                           "Setting variable value",
                           var->name.c_str(),
                           ". This variable is immutable.");
        return;
    }
}

int DataInitJS::helperFunc(int dim_num)
{
    if (dim_num == (max_dims->size()-1)) return 1;
    int acc=1;
    for (int i=dim_num+1; i<max_dims->size();i++)
    {
        acc *= max_dims->at(i);
    }
    return acc;
}

int DataInitJS::getTotalSize()
{
    int acc=1;
    for (int i=0; i<max_dims->size();i++)
    {
        acc *= max_dims->at(i);
    }
    return acc;
}

//void DataInitJS::init_get_m()
//{
//    processing_type.insert(
//                std::pair<int, std::function<double(DataInitJS *, std::vector<int>)>>
//                (NC_BYTE, (&DataInitJS::shell_getElem<char>))
//                );
//    processing_type.insert(
//                std::pair<int, std::function<double(DataInitJS *,std::vector<int>)>>
//                (NC_CHAR, (&DataInitJS::shell_getElem<int8_t>))
//                );
//    processing_type.insert(
//                std::pair<int, std::function<double(DataInitJS *,std::vector<int>)>>
//                (NC_SHORT, (&DataInitJS::shell_getElem<int16_t>))
//                );
//    processing_type.insert(
//                std::pair<int, std::function<double(DataInitJS *, std::vector<int>)>>
//                (NC_INT, (&DataInitJS::shell_getElem<int32_t>))
//                );
//    processing_type.insert(
//                std::pair<int, std::function<double(DataInitJS *, std::vector<int>)>>
//                (NC_FLOAT, (&DataInitJS::shell_getElem<float>))
//                );
//    processing_type.insert(
//                std::pair<int, std::function<double(DataInitJS *,std::vector<int>)>>
//                (NC_DOUBLE, (&DataInitJS::shell_getElem<double>))
//                );
//    processing_type.insert(
//                std::pair<int, std::function<double(DataInitJS *,std::vector<int>)>>
//                (NC_BYTE, (&DataInitJS::shell_getElem<uint8_t>))
//                );
//    processing_type.insert(
//                std::pair<int, std::function<double(DataInitJS *,std::vector<int>)>>
//                (NC_USHORT, (&DataInitJS::shell_getElem<uint16_t>))
//                );
//    processing_type.insert(
//                std::pair<int, std::function<double(DataInitJS *,std::vector<int>)>>
//                (NC_UINT, (&DataInitJS::shell_getElem<uint32_t>))
//                );
//    processing_type.insert(
//                std::pair<int, std::function<double(DataInitJS *,std::vector<int>)>>
//                (NC_INT64, (&DataInitJS::shell_getElem<int64_t>))
//                );
//    processing_type.insert(
//                std::pair<int, std::function<double(DataInitJS *,std::vector<int>)>>
//                (NC_UINT64, (&DataInitJS::shell_getElem<uint64_t>))
//                );
//}
