#pragma once
//#include "Model.h"
#include <fstream>
#include <vector>
#include "ReaderNCFile.h"
#include "configparser.h"
double m_pi_p=3.14;
uint64_t Epoch_timestamp = 946684800;
uint64_t year = 31536000;
const int TOTAL_J1GDR_HEADER_CHARS = 9410; // ������ � ������ ������������ �����

//AnsiString Track;
//AnsiString FileType;
//TDateTime StartDat;
//TDateTime EndDat;
//TStringList* StrLtSSH;
//TStringList* StrLtSWH;
//TStringList* StrLtAAA;
//������� ������ ERF(X)
double erf(double x) {
    double a = -8.0 / 3.0 / m_pi_p * (m_pi_p - 3.0) / (m_pi_p - 4.0);
    if (x >= 0)return sqrt(1.0 - exp(-1.0*x*x*(4.0 / m_pi_p + a * x*x) / (1.0 + a * x*x)));
    else    return -1.0*sqrt(1.0 - exp(-1.0*x*x*(4.0 / m_pi_p + a * x*x) / (1.0 + a * x*x)));
}

auto read_data(std::vector<variable> variable_list, std::string name)
{

	for (auto i : variable_list)
	{
		if (i.name == name)
			return i;
	}
	/*switch (var->type)
	{
	case NC_BYTE:
		temp = new char[i_mix];
		break;
	case NC_CHAR:
		temp = new int8_t[i_mix];
		break;

	case NC_SHORT:
		temp = new int16_t[i_mix];
		break;
	case NC_INT:
		temp = new int32_t[i_mix];
		break;
	case NC_FLOAT:
		temp = new float[i_mix];
		break;
	case NC_DOUBLE:
		temp = new double[i_mix];
		break;
	case NC_UBYTE:
		temp = new uint8_t[i_mix];
		break;
	case NC_USHORT:
		temp = new uint16_t[i_mix];
		break;
	case NC_UINT:
		temp = new uint32_t[i_mix];
		break;
	case NC_INT64:
		temp = new int64_t[i_mix];
		break;
	case NC_UINT64:
		temp = new uint64_t[i_mix];
		break;
	default:
		break;
	}*/
}


void Retracking(reader_nc_file *reader_nc, struct_conf_f *conf) {
	int cycle;
	int  nrec, numrecs, datarecs, p, l, start_of_data_offset = TOTAL_J1GDR_HEADER_CHARS;
	long offset;
	float ww0 = 0;          // ���������
	float wur = 5;          // �����
	int const nu = 4;		  //����������� ����� ��� ������������ ��������� ������
	float dl, dhl, ssh, SF;
    int k = 0;
	// ���� �� ������
	//for (cycle = StartCycle; cycle <= EndCycle; cycle++) 
	//{

	
		//��������� �������� ����� �� ����� � �����
		/*FileType = Form1->DBWay + "JA1_SDR_2PcP";
		if (IntToStr(cycle).Length() == 2) FileType += "0" + IntToStr(cycle);
		else if (IntToStr(cycle).Length() == 1)  FileType += "00" + IntToStr(cycle);
		else  FileType += IntToStr(cycle);
		FileType += "_" + Form1->ComBoxTrack1->Text;*/
		//if(IntToStr(cycle).Length()==2) FileType.Insert("0"+IntToStr(cycle),13);
		//else if(IntToStr(cycle).Length()==1) FileType.Insert("00"+IntToStr(cycle),13);
		//else  FileType.Insert(IntToStr(cycle),13);
	/*	if (Form1->StatusBar1->SimpleText == "Reading .")
			Form1->StatusBar1->SimpleText = "Reading . .";
		else if (Form1->StatusBar1->SimpleText == "Reading . .")
			Form1->StatusBar1->SimpleText = "Reading . . .";
		else if (Form1->StatusBar1->SimpleText == "Reading")
			Form1->StatusBar1->SimpleText = "Reading .";
		else Form1->StatusBar1->SimpleText = "Reading";*/
		//��������� ����� �� ������
		//ifstream fin(FileType.c_str(), ios::binary);
		//if (!fin) { fin.close(); continue; }
		//fin.seekg(0, ios::end);											// ������� ��������� � ����� �����
		//offset = fin.tellg();                                            // ��������� ������ �����
		//datarecs = (offset - start_of_data_offset) / J1GDR_RECSIZE;    // ����� �����(�������)
		//fin.seekg(0, ios::beg);                                          // ������� ��������� � ������ �����
		//offset = start_of_data_offset;                                 // ����������� ��������� ����� ����� ������  1�� �����
		//fin.seekg(offset, ios::beg);                              	 // �������� ��������� �� ����� ����� ������ 1�� ����

		// ������� ��������� ������� ���� TStringList � �������������� ���������� ����������
		std::vector<float> SSH;
		std::vector<float> SWH;
		std::vector<float> AAA;
		//get time
        auto iter = read_data(reader_nc->variable_list, "time");
		double *time = static_cast<double *>(iter.array);
        //std::cout << *static_cast<double *>(time) << std::endl;

        iter = read_data(reader_nc->variable_list, "lat");
		int *lat = static_cast<int *>(iter.array);
        //std::cout << *static_cast<int *>(lat) << std::endl;

        iter = read_data(reader_nc->variable_list, "lon");
        int *lon = static_cast<int *>(iter.array);
        //std::cout << *static_cast<float *>(lon) << std::endl;

        iter = read_data(reader_nc->variable_list, "model_dry_tropo_corr"); //ok
		int16_t *model_dry_tropo_corr = static_cast<int16_t *>(iter.array);
        //std::cout << *static_cast<int16_t *>(model_dry_tropo_corr) << std::endl;

        iter = read_data(reader_nc->variable_list, "model_wet_tropo_corr");//ok
		int16_t *model_wet_tropo_corr = static_cast<int16_t *>(iter.array);
        //std::cout << *static_cast<int16_t *>(model_wet_tropo_corr) << std::endl;

        iter = read_data(reader_nc->variable_list, "iono_corr_alt_ku");//ok
		int16_t *iono_corr_doris_ku = static_cast<int16_t *>(iter.array);
        //std::cout << *static_cast<int16_t *>(iono_corr_doris_ku) << std::endl;

        iter = read_data(reader_nc->variable_list, "sig0_20hz_ku");//ok
		int16_t *K_Cal_Ku_factor_sig0_ku = static_cast<int16_t *>(iter.array);
        //std::cout << *static_cast<int16_t *>(K_Cal_Ku_factor_sig0_ku) << std::endl;

        iter = read_data(reader_nc->variable_list, "sig0_ku");//ok
		int16_t *cal_corr_sig0_ku = static_cast<int16_t *>(iter.array);
        //std::cout << *static_cast<int16_t *>(cal_corr_sig0_ku) << std::endl;

        iter = read_data(reader_nc->variable_list, "net_instr_corr_sig0_ku");//ok
		int16_t *instr_corr_sig0_ku = static_cast<int16_t *>(iter.array);
        //std::cout << *static_cast<int16_t *>(instr_corr_sig0_ku) << std::endl;

        iter = read_data(reader_nc->variable_list, "waveforms_20hz_ku");//ok
		int16_t *waveforms_ku = static_cast<int16_t *>(iter.array);
        //std::cout << *static_cast<int16_t *>(waveforms_ku) << std::endl;

        iter = read_data(reader_nc->variable_list, "modeled_instr_corr_swh_ku");//ok
		int16_t *instr_corr_Swh_ku = static_cast<int16_t *>(iter.array);
        //std::cout << *static_cast<int16_t *>(instr_corr_Swh_ku) << std::endl;

        iter = read_data(reader_nc->variable_list, "alt");
		int *altitude = static_cast<int *>(iter.array);
        //std::cout << *static_cast<int *>(altitude) << std::endl;
		
        iter = read_data(reader_nc->variable_list, "orb_alt_rate");//ok
		int16_t *alt_hi_rate = static_cast<int16_t *>(iter.array);
        //std::cout << *static_cast<int16_t *>(alt_hi_rate) << std::endl;
		/*auto iter = read_data(reader_nc.variable_list, "net_instr_corr_sig0_ku");
		double *time = static_cast<double *>(iter.array);*/
        int max = reader_nc->dimension_list.at(0).value;

		// ���� �� ������ 1 ��
        for (int nrec = 0; nrec <= max; nrec++)
		{
            uint64_t dat = Epoch_timestamp + int(time[nrec]) + year*2;
			//decode_data(fin);                    //���������� ������ � ����������� ���������

			// �������� ����� ���������������� ��������
			/*	if((((int)j1gdr.qual_1hz_alt_instr_corr)%2)){
					offset += J1GDR_RECSIZE;
					fin.seekg(offset, ios::beg);
					continue;
			}
			*/
			//������ �� ����
//			if (conf.StartDat>dat || conf.EndDat<dat)
//			{
//				continue;
//			}
            //������ 1��-� ����� �� �����������
            float itt = lat[nrec];
            float irr = lon[nrec];
//            if ((lat[nrec]< 66083170) || (lat[nrec] > 66081851) || (lon[nrec] < 30411785) || (lon[nrec] > 31090127)) {
//                continue;
//            }


			// ���� �� 20 �� ������
			for (int i = 0; i < 20; i++) 
			{

				// ������  20��-� �� �������������� ���������
//                if ((lat[i] < conf->Lat1) || (lat[i] > conf->Lat2) || (lon[i] < conf->Lon1) || (lon[i] > conf->Lon2)) {
//					continue;
//				}

				//��������� ���������
				// ������ ������ �� ������ �� �����
                ssh = 0.01*(altitude[i+nrec*20] + alt_hi_rate[i+nrec*20] /*- rel_range_ku[i]*/ - model_dry_tropo_corr[nrec] - model_wet_tropo_corr[nrec] - iono_corr_doris_ku[nrec]);
                SF = pow(10, K_Cal_Ku_factor_sig0_ku[i+nrec*20] / 1000 + cal_corr_sig0_ku[nrec] / 1000 + instr_corr_sig0_ku[nrec]/ 1000);
				// ������ ����������
				ww0 = 0;
                int j = 0;
                for (j= 0; j < 15; j++){
                    int k = j + (i*104)+(nrec*20*104);
                    if(k>max)break;
                    ww0 = ww0 + SF * waveforms_ku[k];
                }
				ww0 /= 15.;
				// ����� ����� ������� �� ������
				//wur=wur*pow(10,j1gdr.K_Cal_Ku_factor_sig0_ku[i]/1000.)*2;

                for (j = 0; j < 104; j++) {
                    k = j + (i*104)+(nrec*20*104);
                    if(k>max)break;
                    if ((SF*waveforms_ku[k] > (ww0 + wur)) && (SF*waveforms_ku[k+3] > (ww0 + wur)))
						break;
				}

				// ������ ��������  �� 32.5 �����
                k = j + (i*104)+(nrec*20*104);
                if ((j > 1) && (waveforms_ku[k] != waveforms_ku[k-1])) {
                    dl = j - 32.5;
                    dhl = -(SF*waveforms_ku[k] - wur - ww0) / (SF*waveforms_ku[k] - SF * waveforms_ku[k-1]);
				}
				else { dl = 0; dhl = 0; }

				//�������� ������ (���� ���������� ������)
				/*if(fabs(dhl)<104) ssh=ssh-(dl+dhl)*46.8;
				fout<<ssh<<endl;
				*/

				float WF[nu];      //������ ��� �������� �����
				int t[nu];         //��������������� ��� ������ �������
				//���������    WF,t
				for (l = 0; l < nu; l++) {
                    t[l] = j + l - nu / 2;
                    int k = t[l] + (i*104)+(nrec*20*104);
                    if(k>max)break;

                    WF[l] = SF * waveforms_ku[k] - ww0;
				}

				// ���������
				float A;
                int L = 0;
				A = WF[0];
				for (l = 0; l < nu; l++)
					if (WF[l] > A) { A = WF[l]; L = l; }
                for (l = 1; l < 15; l++){
                    int k = t[l] + (104 * i)+(nrec*20*104);
                    if(k>max)break;
                    A += SF * waveforms_ku[k] - ww0;
                }
				A /= 15.;
//				if (A > 200) continue; //  ��������� �� ���������
//				if (A <= 0) continue;

				float s;        //�������� �������������

				// �����������  SWH �� ���� �������
		 // /*
				float TG;        // ������� ���� �������
				TG = 0.00001;
				for (l = 1; l < nu; l++)
					if (WF[l] - WF[l - 1] > TG&&WF[l] - WF[l - 1] > 0) TG = WF[l] - WF[l - 1];


                s = A / sqrt(m_pi_p) / TG;
				//if(s<0.289) continue;

					//������������ �������� ������
				int m;
				const int M = 100;
                float  TR[M], Tr = 0.0;
				float B[nu], dB[M];
				float C = 100000000;

				for (m = 0; m < M; m++) {
					TR[m] = t[0] + (t[nu - 1] - t[0])*m / M;
					dB[m] = 0;
					for (l = 0; l < nu; l++) {
						B[l] = fabs(WF[l] - 2 * A*(1 + erf((t[l] - TR[m]) / s)));
						dB[m] += B[l];
					}
					dB[m] /= float(nu);
					if (dB[m] < C) { C = dB[m]; Tr = TR[m]; }
				}

				//���������� ��������� ���������
				 /*
						const int M=100,N=100;
						int m,n;
						float S[N], TR[M],Tr;
						float B[nu], dB[N][M];
						float C=1000000;
						for(n=0;n<N;n++){
							S[n]=2*(1+n)/float(N);
							for(m=0;m<M;m++){
								TR[m]=t[0]+(t[nu-1]-t[0])*m/float(M);
								dB[n][m]=0;
								for(l=0;l<nu;l++){
									B[l]=fabs(WF[l]-2*A*(1+erf((t[l]-TR[m])/S[n])));
									dB[n][m]+=B[l];
								}
								dB[n][m]/=float(nu);
								if(dB[n][m]<C){C=dB[n][m]; Tr=TR[m]; s=S[n];}
							}
						}
						//if(s<0.289) continue;
				   */

				//�������� ������
				ssh = ssh - (Tr - 32.5)*46.8;
				//ssh=pow(10,j1gdr.K_Cal_Ku_factor_sig0_ku[i]/1000.);

				float Swh;      //SWH
                Swh = 4 * 46.875*sqrt(fabs(s*s / 2. - 0.513*0.513)) + instr_corr_Swh_ku[nrec] / 10.; // �� �������
				Swh = 0.3653846154 * Swh + 1.192307692;          // ����������

                //Swh=46.875*(s*sqrt(m_pi_p)-0.513);             // �� �������
				//Swh=(j1gdr.swh_ku+j1gdr.diff_elem_aver_Swh_ku[i])/10.;                      // �� �����
				//Swh=(j1gdr.swh_ku+j1gdr.diff_elem_aver_Swh_ku[i]+j1gdr.instr_corr_Swh_ku)/10.;   // �� �����+ ��������

			  // ��������� �������� � ��������� ������������� �������� �� ��������� ��������
				//if(Form1->ComBoxSea2->ItemIndex==0) if(ssh<9250||ssh>9700) continue;
				//Chart1->Series[3]->AddXY(j1gdr.DateTime, ssh, "", Chart1->Series[3]->Color);
				SSH.push_back(ssh);

				//if (Form1->ComBoxSea2->ItemIndex == 0) if (Swh > 100 || Swh < 0) continue;
				//	if (j1gdr.lat[i]>=5720801) continue;
				//Chart1->Series[4]->AddXY(j1gdr.DateTime, Swh, "", Chart1->Series[4]->Color);
				SWH.push_back(Swh);


				//A=j1gdr.K_Cal_Ku_factor_sig0_ku[i]/100.;
				A = 10 * log10(A);
				//	A=(j1gdr.sig0_ku+j1gdr.diff_elem_aver_sig0_ku[i])/100.;
				//Chart1->Series[5]->AddXY(j1gdr.DateTime, A, "", Chart1->Series[5]->Color);
				//Chart1->Series[5]->AddXY(j1gdr.lon[i]/1000000.,A,"",Chart1->Series[5]->Color);
				AAA.push_back(A);

			}

			// ������� ��������� � ������ �� ������ ��������� 1�� �����
		//}
		//� ������� ���� TStrings ��������� ���� � ��������� �� ���� ��������� ������� TStrings
	/*	if (!SSH.empty()) StrLtSSH->AddObject(j1gdr.DateTime, SSH);
		if (!SWH.empty()) StrLtSWH->AddObject(j1gdr.DateTime, SWH);
		if (!AAA.empty()) StrLtAAA->AddObject(j1gdr.DateTime, AAA);*/
			
	}
	std::ofstream ofs;
    ofs.open("out.txt");
	if (!ofs)
	{
        //std::cout << "Open input file error" << std::endl;
		return;
	}
	for (int i = 0; i < max; i++)
	{
        if ((i>SSH.size()-1)||(i>SWH.size()-1)||(i>AAA.size()-1)) break;
		ofs << SSH[i] << "\t" << SWH[i] << "\t" << AAA[i] << std::endl;
	}
	ofs.close();
	//���������� � ��������� � �������
	float Temp, Av = 0, Av1 = 0, Av2 = 0, Av3 = 0;  // ���������� ��� �������
	int count = 0;                           // ���������� ��� ���������� ������������� ����
	//AnsiString Date;

    //SSH
//    for (i = 0; i < SSH.size(); i++)
//    {
//        Av = 0; Av1 = 0; Av2 = 0; Av3 = 0;
//        count = ((TStringList *)StrLtSSH->Objects[i])->Count;
//        Date = TDateTime(StrLtSSH->Strings[i]).DateString();
//        //������ ����������
//        for (j = 0; j < count; j++) {
//            Temp = StrToFloat(((TStringList*)StrLtSSH->Objects[i])->Strings[j]);
//            Av += Temp / float(count);
//        }
//        // ������ ���������� ��� SSH
//        p = 0;
//        for (j = 0; j < count; j++) {
//            Temp = StrToFloat(((TStringList*)StrLtSSH->Objects[i])->Strings[j]);
//            if (fabs(Temp - Av) < 150) { Av1 += Temp; p++; }
//        }
//        if (p == 0)continue;
//        Av1 /= p;

//        p = 0;
//        for (j = 0; j < count; j++) {
//            Temp = StrToFloat(((TStringList*)StrLtSSH->Objects[i])->Strings[j]);
//            if (fabs(Temp - Av1) < 90) { Av2 += Temp; p++; }
//        }
//        if (p == 0)continue;
//        Av2 /= p;
//        //��������� ����������
//        p = 0;
//        for (j = 0; j < count; j++) {
//            Temp = StrToFloat(((TStringList*)StrLtSSH->Objects[i])->Strings[j]);
//            if (fabs(Temp - Av2) < 30) { Av3 += Temp; p++; }
//        }
//        if (p < 7)continue;
//        Av3 /= p;
//        //������� ������� � �������
//        SSHGrid->Cells[0][SSHGrid->RowCount - 1] = i + 1;
//        SSHGrid->Cells[1][SSHGrid->RowCount - 1] = Date;
//        SSHGrid->Cells[2][SSHGrid->RowCount - 1] = int(Av3);
//        SSHGrid->RowCount++;
//    }
	////SWH
	//for (i = 0; i < SWH.size(); i++)
	//{
	//	Av = 0; Av1 = 0; Av2 = 0; Av3 = 0;
	//	count = ((TStringList *)StrLtSWH->Objects[i])->Count;
	//	Date = TDateTime(StrLtSWH->Strings[i]).DateString();
	//	//������ ����������
	//	for (j = 0; j < count; j++) {
	//		Temp = StrToFloat(((TStringList*)StrLtSWH->Objects[i])->Strings[j]);
	//		Av += Temp / float(count);
	//	}
	//	//������ ����������
	//	p = 0;
	//	for (j = 0; j < count; j++) {
	//		Temp = StrToFloat(((TStringList*)StrLtSWH->Objects[i])->Strings[j]);
	//		if (fabs(Temp - Av) < 20) { Av1 += Temp; p++; }
	//	}
	//	if (p < 5)continue;
	//	Av1 /= p;
	//	//������� ������� � �������
	//	SWHGrid->Cells[0][SWHGrid->RowCount - 1] = i + 1;
	//	SWHGrid->Cells[1][SWHGrid->RowCount - 1] = Date;
	//	SWHGrid->Cells[2][SWHGrid->RowCount - 1] = int(Av1);
	//	SWHGrid->RowCount++;
	//}
	////Amplitude
	//for (i = 0; i < AAA.size(); i++)
	//{
	//	Av = 0; Av1 = 0; Av2 = 0; Av3 = 0;
	//	count = AAA[i];
	//	Date = TDateTime(StrLtAAA->Strings[i]).DateString();
	//	//������ ����������
	//	for (j = 0; j < count; j++) {
	//		Temp = StrToFloat(((TStringList*)StrLtAAA->Objects[i])->Strings[j]);
	//		Av += Temp / float(count);
	//	}
	//	// ������ ����������
	//	p = 0;
	//	for (j = 0; j < count; j++) {
	//		Temp = AAA[j];
	//		if (fabs(Temp - Av) < 20) { Av1 += Temp; p++; }
	//	}
	//	if (p < 5)continue;
	//	Av1 /= p;
	//	//������� ������� � �������
	//	AmplGrid->Cells[0][AmplGrid->RowCount - 1] = i + 1;
	//	AmplGrid->Cells[1][AmplGrid->RowCount - 1] = Date;
	//	AmplGrid->Cells[2][AmplGrid->RowCount - 1] = int(Av1);
	//	AmplGrid->RowCount++;
	//}

}
