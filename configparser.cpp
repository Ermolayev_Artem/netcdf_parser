#include "configparser.h"

bool config_parser::read_file()
{
    config_struct->ifs.open(config_struct->name_conf, std::ios_base::in);
    if (!config_struct->ifs.is_open()) return false;
    std::string config_parameter;
    while (!config_struct->ifs.eof())
    {
        config_parameter.clear();
        std::getline(config_struct->ifs, config_parameter);
        if (!config_parameter.empty())
        {
            config_parameter.erase(std::remove_if(config_parameter.begin(), config_parameter.end(), isspace),
                config_parameter.end());
            if (config_parameter[0] == '#') continue;
            if (!parse_parameter(config_parameter))
            {
                report.send_report(WRONG_CONFIG_LINE, 2, "Read file .conf", config_parameter.c_str());
                return false;
            }
            else continue;
        }
        else continue;
    }
    config_struct->ifs.close();
    return true;
}

bool config_parser::parse_parameter(std::string str)
{
    size_t ptr = str.find_first_of(":");
    if (ptr == std::string::npos)
    {
        return false;
    }
    else
    {
        uint8_t lenght_parameter = ptr;
        std::string param_type = str.substr(0, lenght_parameter);
        std::string param_value = str.substr(lenght_parameter+1, str.length());
        // check correct name of parameters
        if (param_type == "att")
        {
            config_struct->list_config.emplace(ATT, param_value);
            return true;
        }
        else if (param_type == "var")
        {
            config_struct->list_config.emplace(VAR, param_value);
            return true;
        }
        else if (param_type == "dim")
        {
            config_struct->list_config.emplace(DIM, param_value);
            return true;
        }
        else if (param_type == "path")
        {
            config_struct->name_conf_nc = param_value;
            return true;
        }
        else if (param_type == "out")
        {
            //TODO read masc
            return true;
        }
        else if (param_type == "lat_b")
        {
            config_struct->Lat1 = std::atoi(param_value.c_str());
            return true;
        }
        else if (param_type == "lat_e")
        {
            config_struct->Lat2 = std::atoi(param_value.c_str());
            return true;
        }
        else if (param_type == "long_b")
        {
            config_struct->Lon1 = std::atoi(param_value.c_str());
            return true;
        }
        else if (param_type == "long_e")
        {
            config_struct->Lon2 = std::atoi(param_value.c_str());
            return true;
        }
        else if (param_type == "time_b")
        {
            config_struct->StartDat = std::atoi(param_value.c_str());
            return true;
        }
        else if (param_type == "time_e")
        {
            config_struct->EndDat = std::atoi(param_value.c_str());
            return true;
        }
//        else if (param_type == "debug")
//        {
//            config_struct.DB = std::atoi(param_value.c_str());
//            return true;
//        }
        else if (param_type == "cycle")
        {
            return true;
        }
        else if (param_type == "track1")
        {
            return true;
        }
        else if (param_type == "track2")
        {
            return true;
        }
        else return false;
    }
}

const std::string config_parser::get_name_conf_nc()
{
    return config_struct->name_conf_nc;
}

void config_parser::create_default_config_on_disc()
{
    std::ifstream tmp(config_struct->name_conf, std::ios::in);
    if(tmp.is_open()) return;
    config_struct->ifs.open(config_struct->name_conf, std::ios_base::out | std::ios_base::trunc);
    if (!config_struct->ifs.is_open()) return;
    std::vector<std::string> config_parameters;
    config_parameters.push_back("path:C:\\Users\\ermar\\source\\repos\\parser_nasa\\parser_nasa\\test2.nc");
    config_parameters.push_back("lat_b:57174600");
    config_parameters.push_back("lat_e:57298000");
    config_parameters.push_back("long_b:429839000");
    config_parameters.push_back("long_e:432261000");
    config_parameters.push_back("time_b:1009843200");
    config_parameters.push_back("time_e:13253616000");
    config_parameters.push_back("var:time");
    config_parameters.push_back("var:lat");
    config_parameters.push_back("var:lon");
    config_parameters.push_back("var:alt_hi_rate");
    config_parameters.push_back("var:time_20hz");
    config_parameters.push_back("var:surface_type");
    config_parameters.push_back("var:model_dry_tropo_corr");
    config_parameters.push_back("var:model_wet_tropo_corr");
    config_parameters.push_back("var:iono_corr_alt_ku");
    config_parameters.push_back("var:sig0_20hz_ku");
    config_parameters.push_back("var:sig0_ku");
    config_parameters.push_back("var:net_instr_corr_sig0_ku");
    config_parameters.push_back("var:waveforms_20hz_ku");
    config_parameters.push_back("var:modeled_instr_corr_swh_ku");
    config_parameters.push_back("var:alt");
    config_parameters.push_back("var:orb_alt_rate");
    for (auto i: config_parameters)
    {
        config_struct->ifs << i << std::endl;
    }
    config_struct->ifs.close();
    return;
}
