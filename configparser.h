#pragma once
#include <iostream>
#include <cstdlib>
#include <string>
#include <map>
#include <algorithm>
#include <utility>
#include <fstream>
#include "configstruct.h"

class config_parser
{
public:
    struct_conf_f *config_struct = nullptr;
private:
    bool parse_parameter(std::string str);
public:
    config_parser(std::string file_name){
        config_struct = new struct_conf_f();
        config_struct->name_conf = file_name;
    }
    bool              read_file();
    void              create_default_config_on_disc();
    const std::string get_name_conf_nc();
    struct_conf_f     *get_point_struct()
    {
        return config_struct;
    }
    ~config_parser() {}
};

