#ifndef FORM_PARSER_H
#define FORM_PARSER_H

#include <iostream>
#include <vector>

struct var
{
    uint32_t value = 0;
    std::string name;
};

struct map_expr
{
    std::vector <std::pair <char, int>> map;
    size_t len_sym;
    std::vector <var> var_lst;
};

class form_parser
{
    std::vector <map_expr> buf;
    std::vector <std::string> *buf_tmp;
public:
    form_parser(std::string *buf_f){
        size_t pos = 0;
        size_t pos_old = 0;
        while ( pos != std::string::npos)
        {
            while(pos != buf_f->size())
            {
                pos = buf_f->find(";");

                if (pos != std::string::npos)
                {
                    std::string tmp = buf_f->substr(pos_old, pos+1);
                    map_expr map;
                    map.len_sym = tmp.size()-1;
                    create_map_expr(&tmp, &map);

                    //buf.push_back();
                }
                pos_old = pos+1;
            }
        }
    }
    bool create_map_expr(std::string *expr, map_expr *map);
    bool isNormal(const char& ch);
    bool isLetter(const char& ch);
    bool isNumber(const char& ch);
    bool write_val(map_expr *map);

};

#endif // FORM_PARSER_H
