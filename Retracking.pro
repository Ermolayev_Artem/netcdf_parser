#-------------------------------------------------
#
# Project created by QtCreator 2018-12-19T20:55:13
#
#-------------------------------------------------

QT       += core gui qml

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Retracking
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        main.cpp \
        MainWindow.cpp \
    CodeEditor.cpp \
    configeditor.cpp \
    configparser.cpp \
    ReaderNCFfile.cpp \
    DataInitJS.cpp

HEADERS += \
    MainWindow.h \
    CodeEditor.h \
    configeditor.h \
    configstruct.h \
    configparser.h \
    ReaderNCFile.h \
    ReportCodes.h \
    nc_struct.h \
    Retracking.h \
    DataInitJS.h

FORMS += \
    mainwindow.ui \
    configeditor.ui

#LIBS += "C:/mylibs/extra libs/extra.lib"

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    resource.qrc

LIBS += -L"$$PWD/../../../../../Program Files/netCDF 4.6.2/lib/" -lnetcdf

INCLUDEPATH += "$$PWD/../../../../../Program Files/netCDF 4.6.2/include"
DEPENDPATH += "$$PWD/../../../../../Program Files/netCDF 4.6.2/include"
