#include "form_parser.h"

bool form_parser::create_map_expr(std::string *expr, map_expr *map)
{
    std::string buff;
    for (auto i: *expr)
    {
        if (isNumber(i))
        {
            map->map.emplace_back(i, 'n');
        }
        else if (isLetter(i))
        {
            map->map.emplace_back(i,'l');
        }
        else if (isNormal(i))
        {
            map->map.emplace_back(i, i);

        }
    }
    return false;
}

bool form_parser::isNumber(const char& ch)
{
    return ( (ch >= '0' && ch <= '9') || ch == '.');
}

// To judge if a char is letter
bool form_parser::isLetter(const char& ch)
{
    return ( (ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z') );
}

// To judge if a char is normal operator(not a special function operator).
bool form_parser::isNormal(const char& ch)
{
    return ( ch == '+' || ch == '-' || ch == '*' || ch == '/' ||
             ch == '^' || ch == '=' || ch == '(' || ch == ')' ||
             ch == '{' || ch == '}' || ch == '[' || ch == ']');
}

bool form_parser::write_val(map_expr *map)
{
    char let_b;
    bool num_b = false;
//    bool let_b = false;
//    int num;
//    int let;
//    for (auto i: map->map)
//    {
//        if (i.second == 'n')
//        {
//            if(num_b)
//            {
//                num++;
//                let_b = false;
//            }
//        }
//        else if (i.second == 'l'){
//            if()

//        }
//    }
    return num_b;
}

bool is_func(const std::string& str, int& i)
{
    switch(str[i])
    {
        case 's':
        {
            switch(str[i+1])
            {
                case 'i':
                {
                    switch(str[i+2])
                    {
                        case 'n':
                        {
                            switch(str[i+3])
                            {
                                case '(': i += 3; return true;//sin
                                default :
                                {
                                    return false;
                                }
                            }
                        }
                        default:
                        {
                            return false;
                        }
                    }
                }
                case 'q':
                {
                    switch(str[i+2])
                    {
                        case 'r':
                        {
                            switch(str[i+3])
                            {
                                case 't':
                                {
                                    switch(str[i+4])
                                    {
                                        case '(': i += 4; return true;//sqrt
                                        default :
                                        {
                                            return false;
                                        }
                                    }
                                }
                                default:
                                {
                                    return false;
                                }
                            }
                        }
                        default:
                        {
                            return false;
                        }
                    }
                }
                default: return false;
            }
        }
        case 'c':
        {
            switch(str[i+1])
            {
        /*c*/	case 'o':
                {
                    switch(str[i+2])
                    {
                        case 's':
                        {
                            switch(str[i+3])
                            {
                                case '(': i += 3; return true;//cos
                                default : return false;
                            }
                        }
                        case 't':
                        {
                            switch(str[i+3])
                            {
                                case '(': i += 3; return true;//cot
                                default : return false;
                            }
                        }
                        default : return false;
                    }
                }
        /*c*/
                default : return false;
            }
        }
        case 't':
        {
            switch(str[i+1])
            {
                case 'a':
                {
                    switch(str[i+2])
                    {
                        case 'n':
                        {
                            switch(str[i+3])
                            {

                                case '(': i += 3; return false;//tan
                                default : return false;
                            }
                        }
                        default : return false;
                    }
                }
                default : return false;
            }
        }
        case 'f':
        {
            switch(str[i+1])
            {
                case 'o':
                {
                    switch(str[i+2])
                    {
                        case 'r':
                        {
                            switch(str[i+3])
                            {

                                case '(': i += 3; return true;//tan
                                default : return false;
                            }
                        }
                        default: return false;
                    }
                }
                case 'a':
                {
                    switch(str[i+2])
                    {
                        case 'b':
                        {
                            switch(str[i+3])
                            {
                                case 's':
                                {
                                    switch(str[i+4])
                                    {
                                        case '(': i += 4; return true;//tan
                                        default : return false;
                                    }
                                }
                                default: return false;
                            }
                        }
                        default: return false;
                    }
                }
                default: return false;
            }
         }
        case 'l':
        {
            switch(str[i+1])
            {
                case 'o':
                {
                    switch(str[i+2])
                    {
                        case 'g':
                        {
                            switch(str[i+3])
                            {

                                case '(': i += 3; return true;//log 10
                                case 'e':
                                {
                                    switch(str[i+4])
                                    {

                                        case '(': i += 4; return true;//log e
                                        default: return false;
                                    }
                                }
                                default: return false;
                            }
                        }
                        default : return false;
                    }
                }
                default: return false;
            }
        }
        default: return false;
    }
}
