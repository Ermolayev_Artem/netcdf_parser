#include "ReaderNCFile.h"

bool reader_conf_nc_file::read_file()
{
    if(is_one) return false;
	ifs.open(name_conf_nc, std::ifstream::out);
	// check file is open
    if (!ifs.is_open())	report.send_report(ERROR_OPEN_FILE, 1, "Read file *nc.conf.");
	std::string config_parameter;

	while (!ifs.eof())
	{
		std::getline(ifs, config_parameter);
		if (!config_parameter.empty()) // 
			name_conf_nc_list.push_back(config_parameter);
		else continue; // might stumble on space or line break
	}
	ifs.close();

//	if (name_conf_nc_list.empty())
//	{
//		report.send_report(WRONG_CONFIG_LINE, 2, &std::string("Read file *nc.conf."), &std::string("File is empty."));
//		return false;
//	}
	return true;
}
//int reader_nc_file::analize()
//{
//	nc_inq_varndims(descr_f,)
//}
template <class T>
bool reader_nc_file::shell_get_var_func(reader_nc_file* A, variable* var)
{
    return A->get_var_val<T>(var);
}

void reader_nc_file::init_reader()
{
    processing_type.insert(
                std::pair<int, std::function<bool(reader_nc_file*,variable*)>>
                (NC_BYTE, (&reader_nc_file::shell_get_var_func<char>))
                );
    processing_type.insert(
                std::pair<int, std::function<bool(reader_nc_file*,variable*)>>
                (NC_CHAR, (&reader_nc_file::shell_get_var_func<int8_t>))
                );
    processing_type.insert(
                std::pair<int, std::function<bool(reader_nc_file*,variable*)>>
                (NC_SHORT, (&reader_nc_file::shell_get_var_func<int16_t>))
                );
    processing_type.insert(
                std::pair<int, std::function<bool(reader_nc_file*,variable*)>>
                (NC_INT, (&reader_nc_file::shell_get_var_func<int32_t>))
                );
    processing_type.insert(
                std::pair<int, std::function<bool(reader_nc_file*,variable*)>>
                (NC_FLOAT, (&reader_nc_file::shell_get_var_func<float>))
                );
    processing_type.insert(
                std::pair<int, std::function<bool(reader_nc_file*,variable*)>>
                (NC_DOUBLE, (&reader_nc_file::shell_get_var_func<double>))
                );
    processing_type.insert(
                std::pair<int, std::function<bool(reader_nc_file*,variable*)>>
                (NC_BYTE, (&reader_nc_file::shell_get_var_func<uint8_t>))
                );
    processing_type.insert(
                std::pair<int, std::function<bool(reader_nc_file*,variable*)>>
                (NC_USHORT, (&reader_nc_file::shell_get_var_func<uint16_t>))
                );
    processing_type.insert(
                std::pair<int, std::function<bool(reader_nc_file*,variable*)>>
                (NC_UINT, (&reader_nc_file::shell_get_var_func<uint32_t>))
                );
    processing_type.insert(
                std::pair<int, std::function<bool(reader_nc_file*,variable*)>>
                (NC_INT64, (&reader_nc_file::shell_get_var_func<int64_t>))
                );
    processing_type.insert(
                std::pair<int, std::function<bool(reader_nc_file*,variable*)>>
                (NC_UINT64, (&reader_nc_file::shell_get_var_func<uint64_t>))
                );
}


bool reader_nc_file::read_all_data_file()
{
	//initialize_lists();
	// open nc file
	int result = nc_open(ncfile_name.c_str(), NC_NOWRITE, &descr_f);
    if (result != NC_NOERR)
    {
        report.send_report(ERROR_NETCDF, 2, "Read *.nc file.", nc_strerror(NC_EBADDIM));
        return false;
    }
	if (!count_parameters())
	{
		nc_close(descr_f);
        return false;
	}
		
	// read all
    if (!__read_info())
    {
        nc_close(descr_f);
        return false;
    }
    if (!read_data_specified_in_config())
	{
		nc_close(descr_f);
		return false;
	}
	nc_close(descr_f);
	return true;
}

void reader_nc_file::__literal_comparison(std::string reference, std::string taste)
{
	// create prompter, it will help us with wrong config variables
//	prompter prom;
//	// first - delim reference
//	prom.create_struct(reference);
//	// second - delim taste
//	prom.create_struct(taste, false);
//	prom.find_diff();
}


void reader_nc_file::read_dim_info()
{
	size_t len = 0;
	char name_in[NC_MAX_NAME] = { 0 };
    for (int i = 0; i < dimensions_count; i++)
	{
		int result = nc_inq_dim(descr_f, i, name_in, &len);
        if (result != NC_NOERR)
            report.send_report(ERROR_NETCDF, 2, "Read *.nc file.", nc_strerror(result));
        dimension_list.emplace_back(dimension(name_in, i, len));
	}
}

bool reader_nc_file::read_attr_info()
{
	size_t len = 0;
	char name_in[NC_MAX_NAME] = { 0 };
    for (int i = 0; i < attributes_count; i++)
	{
		// check the global attributes
		nc_type xtypep;
		int result = nc_inq_attname(descr_f, NC_GLOBAL, i, name_in);
        if (result != NC_NOERR)
            report.send_report(ERROR_NETCDF, 2, "Read *.nc file.", nc_strerror(result));
        else
        {
            result = nc_inq_att(descr_f, NC_GLOBAL, name_in, &xtypep, &len);
            if (result != NC_NOERR)
            {
                report.send_report(ERROR_NETCDF, 2, "Read *.nc file.", nc_strerror(result));
                return false;
            }
            else attribute_list.emplace_back(attribute(name_in, NC_GLOBAL, xtypep, len));
        }
    }
    return true;
}

void reader_nc_file::read_var_info()
{
	int  	ndimsp = 0;
	int  	nattsp = 0;
	nc_type xtypep = NC_NAT;
	int  	dimidsp[MAX_NC_DIMS]/* = { 0 }*/;
	char    name_in[NC_MAX_NAME] = { 0 };
    for (int i = 0; i < variables_count; i++)
	{
		int result = nc_inq_var(descr_f, i, name_in, &xtypep, &ndimsp, dimidsp, &nattsp);
        if (result != NC_NOERR)
            report.send_report(ERROR_NETCDF, 2, "Read *.nc file.", nc_strerror(result));
        variable_list.emplace_back(variable(name_in, i, xtypep, ndimsp, nattsp, dimidsp));
    }
}

bool reader_nc_file::__read_info()
{
    read_dim_info();
    read_attr_info();
    read_var_info();
    init_reader();
	return true;
}

int reader_nc_file::get_att_val(int attid, attribute *att)
{
	int result = 0;
	switch (att->type)
	{
	case NC_BYTE:
	{
		//att->array = static_cast<char*>(att->array);
		char *temp_buf_c = new char[att->lenght];
		temp_buf_c[att->lenght] = '\0';
		//temp_buf = temp_buf_c;
		/*std::cout << temp_buf_c << std::endl;
		std::cout << static_cast<char*>(temp_buf) << std::endl;*/
		result = nc_get_att(descr_f, attid, att->name.c_str(), temp_buf_c);
		att->array = temp_buf_c;
		//std::cout << temp_buf_c << std::endl;
		//std::cout << static_cast<char*>(att->array) << std::endl;

		//std::cout << static_cast<int8_t*>(temp_buf_c) << std::endl;
		//std::cout << *static_cast<char*>(temp_buf) << std::endl;
		//std::cout << static_cast<int8_t*>(att->array) << std::endl;
		return result;
	}
	case NC_CHAR:
	{
		int8_t *att_char = new int8_t[att->lenght];
		att_char[att->lenght] = '\0';
		result = nc_get_att(descr_f, attid, att->name.c_str(), att_char);
		att->array = att_char;
		return result;
	}
	case NC_SHORT:
	{
		int16_t *att_short = new int16_t[att->lenght];
		att_short[att->lenght] = '\0';
		result = nc_get_att(descr_f, attid, att->name.c_str(), att_short);
		att->array = att_short;
		return result;
	}
	case NC_INT:
	{
		int32_t *att_int = new int32_t[att->lenght];
		att_int[att->lenght] = '\0';
		result = nc_get_att(descr_f, attid, att->name.c_str(), att_int);
		att->array = att_int;
		return result;
	}
	case NC_FLOAT:
	{
		float *att_float = new float[att->lenght];
		att_float[att->lenght] = '\0';
		result = nc_get_att(descr_f, attid, att->name.c_str(), att_float);
		att->array = att_float;
		return result;
	}
	case NC_DOUBLE:
	{
		double *att_double = new double[att->lenght];
		att_double[att->lenght] = '\0';
		result = nc_get_att(descr_f, attid, att->name.c_str(), att_double);
		att->array = att_double;
		return result;
	}
	case NC_UBYTE:
	{
		uint8_t *att_ubyte = new uint8_t[att->lenght];
		att_ubyte[att->lenght] = '\0';
		result = nc_get_att(descr_f, attid, att->name.c_str(), att_ubyte);
		att->array = att_ubyte;
		return result;
	}
	case NC_USHORT:
	{
		uint16_t *att_ushort = new uint16_t[att->lenght];
		att_ushort[att->lenght] = '\0';
		result = nc_get_att(descr_f, attid, att->name.c_str(), att_ushort);
		att->array = att_ushort;
		return result;
	}
	case NC_UINT:
	{
		uint32_t *att_uint = new uint32_t[att->lenght];
		att_uint[att->lenght] = '\0';
		result = nc_get_att(descr_f, attid, att->name.c_str(), att_uint);
		att->array = att_uint;
		return result;
	}
	case NC_INT64:
	{
		int64_t *att_int64 = new int64_t[att->lenght];
		att_int64[att->lenght] = '\0';
		result = nc_get_att(descr_f, attid, att->name.c_str(), att_int64);
		att->array = att_int64;
		return result;
	}
	case NC_UINT64:
	{
		uint64_t *att_uint64 = new uint64_t[att->lenght];
		att_uint64[att->lenght] = '\0';
		result = nc_get_att(descr_f, attid, att->name.c_str(), att_uint64);
		att->array = att_uint64;
		return result;
	}
	case NC_STRING:
	{
		char **att_string = new char*[att->lenght];
		att_string[att->lenght] = '\0';
		result = nc_get_att(descr_f, attid, att->name.c_str(), att_string);
		att->array = att_string;
		return result;
	}
	default:
		break;
	}
}

template <class T>
bool reader_nc_file::get_var_val(variable *var)
{
    int result = 0;
    size_t i_mix = 1;
    void *temp = nullptr;
    //���������� ����� ��������� ������ � ����������
    for (auto it : dimension_list)
        for (int i = 0; i < var->dim_count; i++)
            if (it.id == var->num_dim[i])
            {
                i_mix *= it.value;
                var->dim_arr.push_back(it);
                break;
            }
    var->size_arr = i_mix;

    if (i_mix == 0)
    {
        report.send_report(ERROR_ALGORITHM, 1, "Find dimension value in config rule.");
        return false;
    }

    temp = new T[i_mix];
    if (temp == nullptr)
    {
        report.send_report(ERROR_ALGORITHM, 1, "Allocate memory.");
        return false;
    }

    result = nc_get_var(descr_f, var->id, temp);
    if (result != NC_NOERR)
    {
        report.send_report(ERROR_NETCDF, 2, "Find variable in config rule.", nc_strerror(result));
        return false;
    }

    var->check = true;
    var->array = temp;

    return true;
}

bool reader_nc_file::read_data_specified_in_config()
{
//    InterStorageP writerIPoint;
//    writerIPoint.set_var_list_p(&variable_list);
	for (auto i : list_config)
	{
		switch (i.first)
		{
		case ATT:
			for (auto &j : attribute_list)
			{
				if (j.check) continue;
				if (i.second == j.name)
				{
					// ��������� ����������
					void* ip = nullptr;
					int result = get_att_val(NC_GLOBAL, &j);
                    if (result != NC_NOERR)
                        report.send_report(ERROR_NETCDF, 2, "Find attribute in config rule.", nc_strerror(result));
					j.check = true;
					break;
				}
				else
				{
					__literal_comparison(j.name, i.second);
				}
			}
			break;
		case VAR:
			for (auto &j : variable_list)
			{
				if (j.check) continue;
				if (i.second == j.name)
				{
					/*if(processing_type.find(j.type) != processing_type.end())
						return false;*/
                    auto it_func = processing_type.find(j.type);
                    if (it_func == processing_type.end())
                    {
                        report.send_report(ERROR_ALGORITHM, 3,
                                           "Allocate memory by type of variable.",
                                           "Type of variable",
                                           std::to_string(j.type).c_str());
                        return false;
                    }
                    if (!(it_func->second(this, &j)))
                        return false;
                    variable_conf.push_back(&j);
                    break;
				}
				else __literal_comparison(j.name, i.second);
			}
			break;
		default:
			break;
		}
	}
    return true;
}

bool reader_nc_file::count_parameters()
{
	// ��������� ���������� �������� � netcdf �����
	int result = nc_inq(descr_f, &dimensions_count, &variables_count, &attributes_count, &unlim_dimension_count);
    report.send_report(DEBUG, 6, "dimensions_count=", std::to_string(dimensions_count).c_str(),
                       "variables_count=", std::to_string(variables_count).c_str(),
                       "attributes_count=", std::to_string(attributes_count).c_str());
    if (result != NC_NOERR)
    {
        report.send_report(ERROR_NETCDF, 2, "�ount the number of objects in netcdf.", nc_strerror(result));
        return false;
    }
	return true;
}

//bool reader_nc_file::get_var_val(int varid, variable *var, uint16_t count)
//{
//	int result = 0;
//	//int i_max = 0;
//	//int j_max = 0;
//	size_t i_mix = 1;
//	bool k_max = 0;
//	void *temp = nullptr;
//	//���������� ����� ��������� ������ � ����������
//	for (auto it : dimension_list)
//        for (int i = 0; i < var->dim_count; i++)
//            if (it.id == var->num_dim[i])
//            {
//				i_mix *= it.value;
//                break;
//            }
//    var->size_arr = i_mix;

//	switch (var->type)
//	{
//	case NC_BYTE:
//		temp = new char[i_mix];
//		break;
//	case NC_CHAR:
//		temp = new int8_t[i_mix];
//		break;
//	case NC_SHORT:
//		temp = new int16_t[i_mix];
//		break;
//	case NC_INT:
//		temp = new int[i_mix];
//		break;
//	case NC_FLOAT:
//		temp = new float[i_mix];
//		break;
//	case NC_DOUBLE:
//		temp = new double[i_mix];
//		break;
//	case NC_UBYTE:
//		temp = new uint8_t[i_mix];
//		break;
//	case NC_USHORT:
//		temp = new uint16_t[i_mix];
//		break;
//	case NC_UINT:
//		temp = new uint32_t[i_mix];
//		break;
//	case NC_INT64:
//		temp = new int64_t[i_mix];
//		break;
//	case NC_UINT64:
//		temp = new uint64_t[i_mix];
//		break;
//	default:
//		break;
//	}

//    if (temp == nullptr)
//    {
//        report.send_report(ERROR_ALGORITHM, 1, "Allocate memory.");
//        return false;
//    }
//	result = nc_get_var(descr_f, var->id, temp);
//    if (result != NC_NOERR)
//    {
//        report.send_report(ERROR_NETCDF, 2, "Find variable in config rule.", nc_strerror(result));
//        return false;
//    }
//	//int *tp = static_cast<int*>(temp);
//    float tp[120];
//    for (int i = 0; i < 120; i++)
//    {
//        std::cout << tp[i] << std::endl;
//    }
//	//std::cout << *static_cast<int*>(temp) << std::endl;
//	//std::cout << *static_cast<float*>(temp) << std::endl;
//	//std::cout << *static_cast<double*>(temp) << std::endl;
//	////double tmp = *static_cast<int*>(temp);
//	////double tmp1 = *static_cast<double*>(temp);
//	var->check = true;
//	var->array = temp;
//	return true;
//}
template<typename T>
void print_ten_val(int count, void * buf)
{
	for (int i = 0; i < count; i++)
	{
		std::cout << static_cast<T*>(buf)[i] << std::endl;
	}
}
reader_nc_file::~reader_nc_file()
{
}
