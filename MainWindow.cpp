#include "MainWindow.h"
#include "ui_mainwindow.h"
#include "Retracking.h"

// TO DELETE
//class AAA : public QObject
//{
//    Q_OBJECT
//public:
////    Q_PROPERTY(int number WRITE passInt READ getInt);
//    std::vector<int> vec;
//    Q_INVOKABLE int getInt(int num)
//    {
//        return vec[num];
//    }

//    Q_INVOKABLE void passInt(int a)
//    {
//        vec.push_back(a);
//    }

//    ~AAA(){}
//};

// TO DELETE OVER

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    report.set_output_format(ui->Messages);
    createMenuBar();
    createToolBar();
    createMessages();
    createCodeEditor();
    connect(this,
            SIGNAL(changeWindowTitle(const QString&)),
            SLOT(slotChangeWindowTitle(const QString&))
           );

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::showEvent(QShowEvent *)
{
    static bool first_show = true;
    if (first_show)
    {
        QList <int> sizs;
        sizs.append(ui->splitter_2->width() * 0.8);
        sizs.append(ui->splitter_2->width() * 0.1);
        sizs.append(ui->splitter_2->width() * 0.1);
        ui->splitter_2->setSizes(sizs);
        first_show = false;
    }
}

void MainWindow::createMessages()
{
    ui->Messages->setReadOnly(true);
    ui->ClearMessages->setIcon(QPixmap(":/clean.png"));
    ui->ClearMessages->setToolTip("Clean log");
    ui->ClearMessages->setIconSize(QSize(16,16));
}

void MainWindow::createCodeEditor()
{
    ui->ced->resize(850,700);   
    ui->pushButton->setToolTip("Run script");
    ui->checkBox->setChecked(true);
}

void MainWindow::createToolBar()
{
    ptb = new QToolBar("Linker ToolBar");
    ptb->addAction(QPixmap(":/file.png"), "New file (Ctrl+N)", this, SLOT(slotNew()));
    ptb->addAction(QPixmap(":/open.png"), "Open file (CTRL+O)", this, SLOT(slotLoad()));
    ptb->addAction(QPixmap(":/save.png"), "Save file (CTRL+S)", this, SLOT(slotSave()));
    ptb->addAction(QPixmap(":/saveas.png"), "Save file as (CTRL+SHIFT+S)", this, SLOT(slotSaveAs()));
    ptb->addSeparator();
//    ptb->addAction(QPixmap(":/paste.png"), "Paste CTRL+V", this, SLOT(ui->ced->paste()));
//    ptb->addAction(QPixmap(":/copy.png"), "Copy CTRL+C", this, SLOT(QPlainTextEdit::copy()));
//    ptb->addAction(QPixmap(":/cut.png"), "Cut CTRL+X", this, SLOT(QPlainTextEdit::cut()));
//    /*ptb->addAction(QPixmap(":/stepout.png"), "Undo typing CTRL+Z", this, QKeySequence("CTRL+Z"));
//    ptb->addAction(QPixmap(":/stepin.png"), "Redo typing CTRL+Y", this, QKeySequence("CTRL+Y"));
//    ptb->addSeparator();*/
    //ptb->addAction(QPixmap(":/options.png"), "Options", this, SLOT(slotNoImpl()));
    addToolBar(Qt::TopToolBarArea, ptb);
}
void MainWindow::createMenuBar()
{
    //create menu
    QMenu*     pmnuFile = new QMenu("&File");
    pmnuFile->addAction("&Open...",
                        this,
                        SLOT(slotLoad()),
                        QKeySequence("CTRL+O")
                       );
    pmnuFile->addAction("&Save",
                        this,
                        SLOT(slotSave()),
                        QKeySequence("CTRL+S")
                       );
    pmnuFile->addAction("&Save As...",
                        this,
                        SLOT(slotSaveAs()),
                        QKeySequence("CTRL+SHIFT+S")
                       );
    pmnuFile->addSeparator();
    pmnuFile->addAction("&Quit",
                        qApp,
                        SLOT(quit()),
                        QKeySequence("CTRL+Q")
                       );
    QMenu*     pmnuEdit = new QMenu("&Edit");
    pmnuEdit->addAction("&Paste",
                        this,
                        SLOT(),
                        QKeySequence("CTRL+V")
                       );
    pmnuEdit->addAction("&Copy",
                        this,
                        SLOT(),
                        QKeySequence("CTRL+C")
                       );
    pmnuEdit->addAction("&Cut",
                        this,
                        SLOT(),
                        QKeySequence("CTRL+X")
                       );
    pmnuEdit->addAction("&Undo typing",
                        this,
                        SLOT(),
                        QKeySequence("CTRL+Z")
                       );
    pmnuEdit->addAction("&Redo typing",
                        this,
                        SLOT(),
                        QKeySequence("CTRL+Y")
                       );

    QMenu*     pmnuConfig = new QMenu("&Config");
    pmnuConfig->addAction("&Edit",
                        this,
                        SLOT(slotConfEdit()),
                        QKeySequence("F5")
                       );
    pmnuConfig->addAction("&Manual editing...",
                        this,
                        SLOT(slotManualEdit()),
                        QKeySequence("F6")
                       );
    pmnuConfig->addAction("&Some Button",
                        this,
                        SLOT(slotNoImpl())
                       );
    QMenu*     pmnuHelp = new QMenu("&Help");
    pmnuHelp->addAction("&About",
                        this,
                        SLOT(slotAbout()),
                        Qt::Key_F1
                       );
    m_p_Act_Button1 = new QAction("Enable debug info", this);
    m_p_Act_Button1->setCheckable(true);
    m_p_Act_Button1->setChecked(false);
    connect(m_p_Act_Button1, SIGNAL(triggered()), this, SLOT(debug_info()));
    pmnuHelp->addAction(m_p_Act_Button1);

    menuBar()->addMenu(pmnuFile);
    menuBar()->addMenu(pmnuEdit);
    menuBar()->addMenu(pmnuConfig);
    menuBar()->addMenu(pmnuHelp);
    menuBar()->setLayoutDirection(Qt::LeftToRight);
}

void MainWindow::debug_info()
{
    if(m_p_Act_Button1->isChecked())
    {
        report.set_debug_enabled();
    }
    else report.unset_debug_enabled();
}

void MainWindow::slotLoad()
{
    int i = ui->ced->toPlainText().size();
    if (i)
        slotWarning();
        {
            QString str = QFileDialog::getOpenFileName();
            if (str.isEmpty()) {
                return;
            }

            QFile file(str);
            if (file.open(QIODevice::ReadOnly)) {
                QTextStream stream(&file);
                ui->ced->setPlainText(stream.readAll());
                file.close();

                m_strFileName = str;
                emit changeWindowTitle(m_strFileName);
            }
            else report.send_report(ERROR_OPEN_FILE, 1, "Load script from file.");
        }
}

// ----------------------------------------------------------------------
bool MainWindow::slotSave()
{
    if (m_strFileName.isEmpty() || (m_strFileName == "untitled"))
    {
        return slotSaveAs();
    }

    QFile file(m_strFileName);
    if (file.open(QIODevice::WriteOnly)) {
        QTextStream(&file) << ui->ced->toPlainText();
        file.close();
        emit changeWindowTitle(m_strFileName);
        return true;
    }
    else
    {
        report.send_report(ERROR_OPEN_FILE, 1, "Save script in file.");
        return false;
    }
}

// ----------------------------------------------------------------------
bool MainWindow::slotSaveAs()
{
    QString str = QFileDialog::getSaveFileName(nullptr, m_strFileName);
    if (!str.isEmpty()) {
        m_strFileName = str;
        return slotSave();
    }
    else return false;
}

// ----------------------------------------------------------------------
void MainWindow::slotConfEdit()
{
    if (cfed == nullptr)
        cfed = new ConfigEditor();

    int res = cfed->exec();
    if (res == QDialog::Accepted)
    {
        ui->VlauesTextBox->clear();
        ui->tabWidget->clear();
        config_struct = cfed->get_point_struct();
        if (config_struct->name_conf_nc.find_last_of(".nc") == std::string::npos)
        {
            conf_nc_file = new reader_conf_nc_file(config_struct->name_conf_nc);
            conf_nc_file->read_file();
            for (auto i: conf_nc_file->name_conf_nc_list)
            {
                nc_file = new reader_nc_file(&config_struct->list_config, (config_struct->name_conf_nc+i));
                if(!nc_file->read_all_data_file())
                    return;
            }
        }
        else {
            nc_file = new reader_nc_file(&config_struct->list_config, config_struct->name_conf_nc);
            if(!nc_file->read_all_data_file())
                return;
            QStringList lst_tab;
            if (!nc_file->dimension_list.empty())
            {
                lst_tab << "Dimension";
                if(!nc_file->attribute_list.empty())
                {
                    lst_tab << "Attribute";
                    if(!nc_file->variable_list.empty())
                    {
                        int i = 0;
                        lst_tab << "Variable";
                        for(; i<3;i++)
                        {
                            {
                                ui->tabWidget->addTab(new QTableWidget(), lst_tab.takeAt(0));
                            }
                        }
                        QStringList lst_tmp;
                        QTableWidgetItem* qtwi;
                        lst_tmp<<"Name"<<"ID"<<"Type"<<"Dimension";

                        QTableWidget * temp_p = (QTableWidget * )ui->tabWidget->widget(2);
                        temp_p->setRowCount(nc_file->variable_list.size());
                        temp_p->setColumnCount(4);
                        temp_p->setHorizontalHeaderLabels(lst_tmp);
                        lst_tmp.clear();
                        i=0;
                        for (auto it: nc_file->variable_list)
                        {
                            qtwi = new QTableWidgetItem(QString(it.name.c_str()));
                            temp_p->setItem(i, 0, qtwi);
                            qtwi = new QTableWidgetItem(QString(std::to_string(it.id).c_str()));
                            temp_p->setItem(i, 1, qtwi);
                            qtwi = new QTableWidgetItem(QString(it.print_type()));
                            temp_p->setItem(i, 2, qtwi);
                            std::string dim;
                            for (auto itt : nc_file->dimension_list)
                                for (int i = 0; i < it.dim_count; i++)
                                    if (itt.id == it.num_dim[i])
                                    {
                                        dim+="["+std::to_string(itt.value)+"]";
                                        break;
                                    }
                            qtwi = new QTableWidgetItem(QString(dim.c_str()));
                            temp_p->setItem(i, 3, qtwi);
                            i++;
                        }
                        temp_p->resizeColumnsToContents();
                        temp_p->setEditTriggers(QAbstractItemView::NoEditTriggers);
                    }
                    else return;
                    QStringList lst_tmp;
                    QTableWidgetItem* qtwi;
                    lst_tmp<<"Name"<<"ID"<<"Type";

                    QTableWidget * temp_p = (QTableWidget * )ui->tabWidget->widget(1);
                    temp_p->setRowCount(nc_file->attribute_list.size());
                    temp_p->setColumnCount(3);
                    temp_p->setHorizontalHeaderLabels(lst_tmp);
                    lst_tmp.clear();
                    int i=0;
                    for (auto it: nc_file->attribute_list)
                    {
                        qtwi = new QTableWidgetItem(QString(it.name.c_str()));
                        temp_p->setItem(i, 0, qtwi);
                        qtwi = new QTableWidgetItem(QString(std::to_string(it.id).c_str()));
                        temp_p->setItem(i, 1, qtwi);
                        qtwi = new QTableWidgetItem(QString(it.print_type()));
                        temp_p->setItem(i, 2, qtwi);
                        i++;
                    }
                    temp_p->resizeColumnsToContents();
                    temp_p->setEditTriggers(QAbstractItemView::NoEditTriggers);
                }
                else return;
                QStringList lst_tmp;
                QTableWidgetItem* qtwi;
                lst_tmp<<"Name"<<"ID"<<"Value";

                QTableWidget * temp_p = (QTableWidget * )ui->tabWidget->widget(0);
                temp_p->setRowCount(nc_file->dimension_list.size());
                temp_p->setColumnCount(3);
                temp_p->setHorizontalHeaderLabels(lst_tmp);
                lst_tmp.clear();
                int i=0;
                for (auto it: nc_file->dimension_list)
                {
                    qtwi = new QTableWidgetItem(QString(it.name.c_str()));
                    temp_p->setItem(i, 0, qtwi);
                    qtwi = new QTableWidgetItem(QString(std::to_string(it.id).c_str()));
                    temp_p->setItem(i, 1, qtwi);
                    qtwi = new QTableWidgetItem(QString(std::to_string(it.value).c_str()));
                    temp_p->setItem(i, 2, qtwi);
                    i++;
                }
                temp_p->resizeColumnsToContents();
                temp_p->setEditTriggers(QAbstractItemView::NoEditTriggers);
            }
            else return;
        }
    }
    else return;
    std::string values_test = "Variables:\n\n";
    for(auto it: nc_file->variable_conf)
    {
        std::string value = it->name;
        for(auto itt: it->get_val_dim())
        {
            value += "[" + std::to_string(itt) + "]";
        }
        values_test+=value+"\n";
    }
    values_test += "\nDimension:\n\n";
    for(auto it: nc_file->dimension_list)
    {
        values_test+="d_"+it.name+"="+std::to_string(it.value)+"\n";
    }
    ui->VlauesTextBox->append(values_test.c_str());
}
// ----------------------------------------------------------------------
void MainWindow::slotNew()
{
    int i = ui->ced->toPlainText().size();
    if (i)
        if (slotWarning())
        {
            ui->ced->clear();
            m_strFileName = "untitled";
            slotChangeWindowTitle(m_strFileName);
        }
}
// ----------------------------------------------------------------------
void MainWindow::slotManualEdit()
{

}
// ----------------------------------------------------------------------
bool MainWindow::slotWarning()
{
    int n =
        QMessageBox::warning(nullptr,
                            "Warning" ,
                            "The text in the file has changed,"
                            "\n Do you want to save the changes?",
                            "Yes",
                            "No",
                            QString(),
                            0,
                            1
                           );
    if (!n) {
        return slotSave();
    }
    else return true;
}

//template<class T>
//static DataInitJS<T>* ret_templ_typeJST(std::vector<int> list)
//{
//    return new DataInitJS<T>(list);
//}
std::string MainWindow::getTextFromForm(QTextEdit* form)
{
    QString str = form->toPlainText();
    std::string str1 = str.toStdString();
    if (str1.empty()) return "";
    for(int i=0; i<str1.size(); i++)
    {
        char a = str1.c_str()[i];
        if ((a < (-1)) || (a >= 255))
        {
            report.send_report(ERROR_TEXT_VALIDATION, 2,
                               "Check text.",
                               "The entered text contains characters that go beyond the ASCII table.");
            return ""; // сремся в лог!!!!
        }
    }
    if (syntax_checker) str1.erase(std::remove_if(str1.begin(), str1.end(), isspace),
                                str1.end());
    return str1;
}
std::vector<std::string> parser_text(std::string delim, std::string str)
{
    report.send_report(DEBUG, 2,
                       "parse text",
                       str.c_str());
    if (str.find_first_of(delim) != std::string::npos)
    {
        std::vector<std::string> vec;
        size_t pos = 0;
        size_t pos_old = pos;
        while (pos != std::string::npos)
        {
            pos = str.find_first_of(delim);
            if (pos == std::string::npos) return vec;
            std::string substr = str.substr(0, pos);
            report.send_report(DEBUG, 2,
                               "parse text",
                               substr.c_str());
            vec.push_back(substr);
            pos_old = pos+1;
            str = str.substr(pos_old, str.size()-pos_old);
        }
        return vec;
    }
    return {""};
}

void MainWindow::on_pushButton_clicked()
{
    std::string str1 = getTextFromForm((QTextEdit*)(ui->ced));
    if (str1.empty()) return;
    std::string str2 = getTextFromForm(ui->OutBoxVal);
    std::vector<std::string> out_vars;
    if (!str2.empty())
    {
        out_vars = parser_text(";", str2);
    }

    //create output variables
    std::vector<DataInitJS*> data_translators;

    if (!out_vars.empty())
    {
        for(auto it: out_vars)
        {
            DataInitJS* temp = new DataInitJS(it);
            data_translators.push_back(temp);
            QJSValue result = scriptEngine.newQObject(temp);
            report.send_report(DEBUG, 2,
                               "set new object",
                               result.toString().toStdString().c_str());
            jsvars.push_back(result);
            scriptEngine.globalObject().setProperty(it.c_str(), jsvars.at(jsvars.size()-1));
            report.send_report(DEBUG, 2,
                               "Set global object to JS",
                               it.c_str());
        }
    }
//    QStringList str_lst;
    std::vector<int> v{1,2,3,4,5,6,7,8,9};
    std::string buf;
    //create input variables
    if (nc_file != nullptr)
    if(!nc_file->variable_conf.empty())
    {
        for(auto &it: nc_file->variable_conf)
        {
            //DataInitJS temp(it);
            //data_translators.emplace_back(it);
            QJSValue result = scriptEngine.newQObject(new DataInitJS(it,false));
            report.send_report(DEBUG, 2,
                               "set new object",
                               result.toString().toStdString().c_str());
            jsvars.push_back(result);
            scriptEngine.globalObject().setProperty(it->name.c_str(), jsvars.at(jsvars.size()-1));
            report.send_report(DEBUG, 2,
                               "Set global object to JS",
                               it->name.c_str());
        }

        for(auto it: nc_file->dimension_list)
        {
            buf+="d_"+it.name+"="+std::to_string(it.value)+";";
        }
    }
    variable var;
    var.array = v.data();
    var.type = NC_INT;
    dimension dim;
    dim.value = 3;
    var.dim_arr.push_back(dim);
    var.dim_arr.push_back(dim);
    DataInitJS temp(&var, true);
    QJSValue result = scriptEngine.newQObject(&temp);
    WriterF writ(std::string("test"), data_translators);
    QJSValue res1 = scriptEngine.newQObject(&writ);
    report.send_report(DEBUG, 3,
                       "Script calculation",
                       str1.c_str(),
                       result.toString().toStdString().c_str());
    scriptEngine.globalObject().setProperty("control_obj", result);

    scriptEngine.globalObject().setProperty("printer", res1);

    //temp.arr = v.data();


//    report.send_report(DEBUG, 2,
//                       "Script calculation",
//                       result.toString().toStdString().c_str());
   // QJSValue res = scriptEngine.evaluate("var res = [[1,2],[3,4]]");

//    report.send_report(DEBUG, 3,
//                       "Script calculation",
//                       str1.c_str(),
//                       res.toString().toStdString().c_str());





    buf+=str1;
    QJSValue val = scriptEngine.evaluate(QString(buf.c_str()));
    report.send_report(OUTPUT, 3,
                       "Script calculation",
                       str1.c_str(),
                       val.toString().toStdString().c_str());





    //form_parser f_parse(&str1);
    //Retracking((nc_file), config_struct);

}

//void MainWindow::dismantler()
//{
//    for(auto it: nc_file->variable_conf)
//    {
//        it->
//    }
//}

void MainWindow::on_ClearMessages_clicked()
{
    ui->Messages->clear();
}

void MainWindow::on_logfile_check_stateChanged(int arg1)
{
    if (arg1 == Qt::CheckState::Checked)
    {
        report.file_mod_enabled();
    }
    else
    {
        report.file_mod_disabled();
    }
}

void MainWindow::on_checkBox_stateChanged(int arg1)
{
    if (arg1 == Qt::CheckState::Checked)
    {
        syntax_checker = true;
    }
    else
    {
        syntax_checker = false;
    }
}
