#ifndef NC_STRUCT_H
#define NC_STRUCT_H

#include <string>
#include <vector>
#include <iostream>
#include <netcdf.h>

struct dimension {
    std::string name;
    int			id;
    size_t		value = 0;
    bool		is_unlimited;
    dimension(std::string name_t, int id_t, size_t lenght_t, bool is_unlimited_t = false) :
        name(name_t), id(id_t), value(lenght_t),  is_unlimited(is_unlimited_t){}
    dimension(){};
};

struct attribute {
    std::string name;
    int			id;
    nc_type		type;
    size_t  	lenght;
    bool		check;
    void		*array = nullptr;
    attribute(std::string name_t, int id_t, nc_type	type_t, size_t lenght_t, bool check_t = false) :
        name(name_t), id(id_t), type(type_t), lenght(lenght_t), check(check_t) {}
    const char* print_type()
    {
        switch (type)
        {
            case NC_BYTE:
                return "char\n";
            case NC_CHAR:
                return "int8_t\n";
            case NC_SHORT:
                return "int16_t\n";
            case NC_INT:
                return "int\n";
            case NC_FLOAT:
                return "float\n";
            case NC_DOUBLE:
                return "double\n";
            case NC_UBYTE:
                return "uint8_t\n";
            case NC_USHORT:
                return "uint16_t\n";
            case NC_UINT:
                return "uint32_t\n";
            case NC_INT64:
                return "int64_t\n";
            case NC_UINT64:
                return "uint64_t\n";
            default:
                return "Unknown type\n";
        }
    }
};

struct variable {
public:
    std::string name;
    size_t      size_arr=0;
    int			id;
    nc_type		type;
    int			dim_count = 0;
    int			attr_count = 0;
    bool		check = false;
    int			num_dim[MAX_NC_DIMS];
    void 		*array = nullptr;
    std::vector<dimension> dim_arr;
    std::vector<attribute> attr_arr;
public:
    variable(){}
    variable(std::string name_t, int id_t, nc_type	type_t, int dim_count_t, int attr_count_t, int *num_dim_t, bool check_t = false) :
        name(name_t), id(id_t), type(type_t), dim_count(dim_count_t), attr_count(attr_count_t), check(check_t)
    {
        int i = 0;
        for (; i < dim_count_t; i++)
        {
            num_dim[i] = num_dim_t[i];
        }
    }
    const char* print_type()
    {
        switch (type)
        {
            case NC_BYTE:
                return "char\n";
            case NC_CHAR:
                return "int8_t\n";
            case NC_SHORT:
                return "int16_t\n";
            case NC_INT:
                return "int\n";
            case NC_FLOAT:
                return "float\n";
            case NC_DOUBLE:
                return "double\n";
            case NC_UBYTE:
                return "uint8_t\n";
            case NC_USHORT:
                return "uint16_t\n";
            case NC_UINT:
                return "uint32_t\n";
            case NC_INT64:
                return "int64_t\n";
            case NC_UINT64:
                return "uint64_t\n";
            default:
                return "Unknown type\n";
        }
    }

    std::vector<int> get_val_dim()
    {
        std::vector<int> val_dim;
        for(auto it:dim_arr)
        {
            val_dim.push_back(it.value);
        }
        return val_dim;
    }
    //std::string long_name;
    //std::string standard_name;
    //std::string units;
    //std::string comment;
    //std::string _CoordinateAxisType;
    //std::string quality_flag;
    //std::string calendar; //TODO: be enum
    //double tai_utc_difference;
    //std::string leap_second; //TODO: be data_format or posix_time
    //std::string coordinates; //TODO: be enum
    //int FillValue; //TODO: be some different types
    //std::string institution;
    //std::string source;
    //std::string flag_meanings;
    //std::string flag_values;
    //std::string valid_min;
    //std::string valid_max;
};

#endif // NC_STRUCT_H
