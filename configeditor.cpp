#include "configeditor.h"
#include "ui_configeditor.h"
#include <QString>

ConfigEditor::ConfigEditor(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ConfigEditor)
{
    ui->setupUi(this);
    ui->UseDefConf->setCheckState(Qt::CheckState::Checked);
    ui->checkBoxCoordinates->setCheckState(Qt::CheckState::Checked);
    QStringList tmp = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
    months = new QStringListModel(tmp);
    ui->beginMonth->setModel(months);
    ui->beginMonth->setEditable(false);
    ui->endMonth->setModel(months);
    ui->endMonth->setEditable(false);
    init_form();
    connect(this,
            SIGNAL(changeWindowTitle(const QString&)),
            SLOT(slotChangeWindowTitle(const QString&))
           );
    //        ui->begin_year->setReadOnly(true);
//            ui->endMonth->setEditable(false);
    //        ui->end_year->setReadOnly(true);
}

void ConfigEditor::set_file_name(std::string file_name = DF_CONFIG_FN)
{

}

ConfigEditor::~ConfigEditor()
{
    delete ui;
}

bool ConfigEditor::write_file()
{
    return false;
}

bool ConfigEditor::init_form()
{
    if (ui->UseDefConf->checkState() == Qt::CheckState::Checked)
    {

        if (ui->checkBoxCoordinates->checkState() != Qt::CheckState::Checked)
        {
            on_checkBoxCoordinates_stateChanged(Qt::CheckState::Checked);
            ui->s_lat_line->setReadOnly(false);
            ui->s_lon_line->setReadOnly(true);
            ui->e_lat_line->setReadOnly(true);
            ui->e_lon_line->setReadOnly(true);
            ui->textEdit_val->setReadOnly(true);
        }
    }
    else
    {
        ui->line_edit_filename->setReadOnly(false);
        ui->pushBtnBrowse->setEnabled(true);
        ui->beginMonth->setEnabled(true);
        ui->begin_year->setReadOnly(false);
        ui->endMonth->setEnabled(true);
        ui->end_year->setReadOnly(false);
        if (ui->UseDefConf->checkState() == Qt::CheckState::Checked)
        {
            ui->s_lat_line->setReadOnly(false);
            ui->s_lon_line->setReadOnly(true);
            ui->e_lat_line->setReadOnly(true);
            ui->e_lon_line->setReadOnly(true);
            ui->textEdit_val->setReadOnly(true);
        }
    }
    return false;
}

void ConfigEditor::on_UseDefConf_stateChanged(int arg1)
{
    if (arg1 == Qt::CheckState::Checked)
    {
        ui->line_edit_filename->setReadOnly(true);
        ui->pushBtnBrowse->setEnabled(false);
        ui->beginMonth->setEnabled(false);
        ui->begin_year->setReadOnly(true);
        ui->endMonth->setEnabled(false);
        ui->end_year->setReadOnly(true);
        ui->textEdit_val->setReadOnly(true);
        ui->checkBoxCoordinates->setEnabled(false);
        if (ui->checkBoxCoordinates->checkState() != Qt::CheckState::Checked)
        {
            on_checkBoxCoordinates_stateChanged(Qt::CheckState::Checked);
        }
    }
    else if (arg1 == Qt::CheckState::Unchecked)
    {
        ui->line_edit_filename->setReadOnly(false);
        ui->pushBtnBrowse->setEnabled(true);
        ui->beginMonth->setEnabled(true);
        ui->begin_year->setReadOnly(false);
        ui->endMonth->setEnabled(true);
        ui->end_year->setReadOnly(false);
        ui->textEdit_val->setReadOnly(false);
        ui->checkBoxCoordinates->setEnabled(true);
    }
}

void ConfigEditor::on_checkBoxCoordinates_stateChanged(int arg1)
{
        if (arg1 == Qt::CheckState::Checked)
        {
            ui->s_lat_line->setReadOnly(true);
            ui->s_lon_line->setReadOnly(true);
            ui->e_lat_line->setReadOnly(true);
            ui->e_lon_line->setReadOnly(true);
        }
        else if (arg1 == Qt::CheckState::Unchecked)
        {
            ui->s_lat_line->setReadOnly(false);
            ui->s_lon_line->setReadOnly(false);
            ui->e_lat_line->setReadOnly(false);
            ui->e_lon_line->setReadOnly(false);
        }
}

void ConfigEditor::on_pushBtnBrowse_clicked()
{
    QString str = QFileDialog::getOpenFileName();
    if (str.isEmpty()) {
        return;
    }
    if (pars == nullptr)
        pars = new config_parser(str.toStdString());
    if (pars->read_file())
    {
        //TODO разобраться со временем
        ui->line_edit_filename->setText(str);
        ui->begin_year->setText("2000");
        ui->end_year->setText("2018");
        std::string tmp;
        for(auto i: pars->config_struct->list_config)
        {
            tmp += i.second +std::string(";");
        }   
        ui->textEdit_val->setText(QString(tmp.c_str()));
        ui->checkBoxCoordinates->setEnabled(true);
//        if (ui->checkBoxCoordinates->checkState() != Qt::CheckState::Checked)
//        {
            ui->s_lat_line->setText(QString(std::to_string(pars->config_struct->Lat1).c_str()));
            ui->s_lon_line->setText(QString(std::to_string(pars->config_struct->Lon1).c_str()));
            ui->e_lat_line->setText(QString(std::to_string(pars->config_struct->Lat2).c_str()));
            ui->e_lon_line->setText(QString(std::to_string(pars->config_struct->Lon2).c_str()));
//        }
    }
    else return;
    emit changeWindowTitle(str);
}

void ConfigEditor::on_pushButtonOK_clicked()
{
    if (ui->UseDefConf->checkState() == Qt::CheckState::Checked)
    {
        pars->~config_parser();
        pars = new config_parser(DF_CONFIG_FN);
        pars->create_default_config_on_disc();
        pars->read_file();
    }
    this->accept();
    this->close();
    return;
}

void ConfigEditor::on_pushButtonCancel_clicked()
{
    this->reject();
    this->close();
}
