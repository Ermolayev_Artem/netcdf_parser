#pragma once

#include <iostream>
#include <fstream>
#include <set>
#include <vector>
#include <cstdarg>
#include <string>
#include <QTextEdit>

//typedef std::vector<std::string>

enum ATTR_CODE //��� ���������� �������
{
	UNKN,
	ATT,
	VAR,
	DIM
	//PATH,
	//OUT,
	//LAT_BEG,
	//LONG_BEG,
	//LAT_END,
	//LONG_END,
	//PER_TIME,
	//CICLE,
	//TRACK1,
	//TRACK2
};

enum CODES
{
	UNKNOWN_CODE,
	DEBUG,
    OUTPUT,
    //
	ERROR_NETCDF,
	ERROR_ALGORITHM,
	ERROR_FILE_NAME,
	WRONG_CONFIG_LINE,
	WRONG_PARAMETER,
	ERROR_OPEN_FILE,
    SUCCESS,
    //
    ERROR_TEXT_VALIDATION,
    ERROR_OUT_OF_RANGE,
    ERROR_SOURCE_DATA
};
class Bug_reports
{
private:
    //specifically for displaying messages in the form
    QTextEdit *Messages;
    std::ofstream fd;
    bool debug_enabled = false;
    bool log_mod;
	std::set <std::pair<CODES, std::string>> reports_list;
	Bug_reports()
	{
		reports_list.emplace(DEBUG, "DEBUG OPERATIONS:");
		reports_list.emplace(ERROR_NETCDF, "Error executing library commands. OPERATIONS:");
		reports_list.emplace(ERROR_ALGORITHM, "The error of the algorithm. It is recommended to contact the developer! OPERATIONS:");
		reports_list.emplace(ERROR_FILE_NAME, "File name error. OPERATIONS:"); 
		reports_list.emplace(WRONG_CONFIG_LINE, "Configuration file parameter error. OPERATIONS:");
		reports_list.emplace(WRONG_PARAMETER, "Unknown parameter. OPERATIONS:");
		reports_list.emplace(ERROR_OPEN_FILE, "Read file error. OPERATIONS:");
		reports_list.emplace(SUCCESS, "Success. OPERATIONS:");
        reports_list.emplace(ERROR_TEXT_VALIDATION, "Errors were found during script validation. OPERATIONS:");
        reports_list.emplace(ERROR_OUT_OF_RANGE, "Error access to array elements. OPERATIONS:");
        reports_list.emplace(ERROR_SOURCE_DATA, "Error when trying to change the source data. OPERATIONS:");
        reports_list.emplace(OUTPUT, "OUTPUT:");
	}
    ~Bug_reports() {
        if(fd.is_open())
            fd.close();
    } // � ����������

	// ���������� ����� ��������� �����������
	Bug_reports(Bug_reports const&) = delete; // ���������� �� �����
	Bug_reports& operator= (Bug_reports const&) = delete;  // � ���
public:
    static Bug_reports& instance()
	{
        static Bug_reports s;
		return s;
	}

//    std::string send_report(CODES code, char *str, ...)
//    {
//        std::string sum;
//        for (auto i : reports_list)
//            if (i.first == code)
//                sum += i.second;
//       char **cp = &str;            //--����� ������� ���������
//       int len = 0;
//       // ���� ��� ����������� ����� ����� ���������� �����
//       while (*cp) { len += strlen(*cp); cp++; }
//      char *s = new char[len+1];       //--������ ��� ������
//      s[0]=0;                       //-- "�������" ������
//    // ���� ��� ��������� �����
//      cp=&str;                       //-- ����� ��������� �� 1-� ��������
//      while (*cp)
//      {
//         strcat(s, *cp);             //-- ���������� ������ (� ���������)
//         cp++;                       //-- ������������ �� ���������
//      }
//      return s;
//    }

//    std::string send_report(CODES code, std::vector<std::string> &buff)
//    {
//        std::string sum;
//        for (auto i : reports_list)
//            if (i.first == code)
//                sum += i.second;
//        for(auto i: buff)
//        {
//            sum += " ";
//            sum += i;
//        }
//        buff.clear();
//        if (console_mode){
//            std::cout<<sum<<std::endl;
//            return "";
//        }
//        else return sum;
//    }
    void set_output_format(QTextEdit *pForm, bool log_enable = false)
    {
        Messages = pForm;
        log_mod = log_enable;
    }
    void send_report(CODES code, uint8_t count,const char  *str, ...)
    {
        if ((code == DEBUG) && (!debug_enabled)) return;
        std::string sum;
        for (auto i : reports_list)
            if (i.first == code)
            {
                sum += i.second;
                break;
            }
        va_list p;             //--���������� ���������
        va_start(p, count);            //--������������� ���������
        while (count--)
        {
            sum += " ";
            sum += va_arg(p, char*);        //--����������� ���������
        }
        va_end(p);                //--��������� ���������

        Messages->append(QString(sum.c_str()));
        if (log_mod)
        {
            fd << sum << std::endl;
        }
    }
    void set_debug_enabled()
    {
        debug_enabled = true;
    }
    void unset_debug_enabled()
    {
        debug_enabled = false;
    }
    void file_mod_enabled()
    {
        log_mod = true;
        fd.open("retracking.log", std::ios_base::app);
        if (!fd.is_open())
        {
            log_mod = false;
            send_report(ERROR_OPEN_FILE, 2, "Open log file.", "Logging to the file is not possible. Check the path to the program, it should not contain non-Latin characters.");
        }
    }
    void file_mod_disabled()
    {
        log_mod = false;
        if (fd.is_open())
            fd.close();
    }
};


static Bug_reports &report = Bug_reports::instance();

