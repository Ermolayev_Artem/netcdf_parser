#ifndef CONFIGEDITOR_H
#define CONFIGEDITOR_H

#include <QtWidgets>
#include <QDialog>
#include <QFile>

#include <fstream>
#include <configstruct.h>
#include <configparser.h>

namespace Ui {
class ConfigEditor;
}

class ConfigEditor : public QDialog
{
    Q_OBJECT
    QStringListModel *months = nullptr;
    config_parser    *pars   = nullptr;
    //struct_conf_f config_struct;
public:
    explicit ConfigEditor(QWidget *parent = nullptr);
    struct_conf_f* get_point_struct()
    {
        return pars->get_point_struct();
    }
    ~ConfigEditor();
private slots:
    void on_UseDefConf_stateChanged(int arg1);

    void on_checkBoxCoordinates_stateChanged(int arg1);

    void on_pushBtnBrowse_clicked();
    void on_pushButtonOK_clicked();

    void on_pushButtonCancel_clicked();

public slots:
    void slotNoImpl()
    {
        QMessageBox::information(nullptr, "Message", "Not implemented");
    }
    void slotChangeWindowTitle(const QString& str)
    {
        setWindowTitle(str);
    }
signals:
    void changeWindowTitle(const QString&);
private:
    //for handler conf
    void set_file_name(std::string file_name);
    bool read_file();
    bool init_form();
    const std::string get_name_conf_nc();
    bool write_file();
    bool parse_parameter(std::string str);
private:
    Ui::ConfigEditor *ui;
};

#endif // CONFIGEDITOR_H
