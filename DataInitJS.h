#ifndef DataInitJS_H
#define DataInitJS_H

#include <QObject>
#include <utility>
#include <functional>

#include "nc_struct.h"
#include "ReportCodes.h"
class DataInitJS;

class WriterF : public QObject
{
    Q_OBJECT

    std::string name;
    std::vector<DataInitJS*> mass_storage;
public:
    explicit WriterF(QObject *parent = nullptr);
    WriterF(std::string str, std::vector<DataInitJS*>& mass_stor);
    ~WriterF(){}
private:
    size_t max_lenght();
public slots:
    void file();
//    void table(std::vector<DataInitJS> mass_storage, std::string name);
//    void console(std::vector<DataInitJS> mass_storage);
};

class DataInitJS : public QObject
{
    Q_OBJECT

    std::vector<int> *max_dims;
    variable *var;
    std::string name;
    bool changeable;
    std::vector<double> output_val;
public:
    explicit DataInitJS(QObject *parent = nullptr);
    DataInitJS(std::string str, bool ch = true);
    DataInitJS(variable *var_t, bool ch = false);

    int helperFunc(int dim_num);
    int getTotalSize();

    double getElem(std::initializer_list<int> list); //cpp method
    double getElem(std::vector<int> list); // js method

    std::string get_varname() const;
    double get_val(size_t num) const;
    size_t get_lenght() const;
    ~DataInitJS();
public slots:
    double get(QList<int> lst);
    void set(double val);
};

#endif // DataInitJS_H
