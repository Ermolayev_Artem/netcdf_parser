#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtWidgets>
#include <QTextCursor>
#include <QJSEngine>
#include "CodeEditor.h"
#include "configeditor.h"
#include "configparser.h"
#include "ReaderNCFile.h"
#include "DataInitJS.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    bool                syntax_checker = true;
    QToolBar            *ptb  = nullptr;
    ConfigEditor        *cfed = nullptr; // форма
    struct_conf_f       *config_struct = nullptr; // структура
    QPointer<QAction>   m_p_Act_Button1 = nullptr;
    QJSEngine scriptEngine;
    std::vector<QJSValue> jsvars;
    QStringListModel    mod;
    QString             m_strFileName;
    reader_conf_nc_file *conf_nc_file = nullptr;
    reader_nc_file      *nc_file = nullptr;
private:
    void createMenuBar   ();
    void createToolBar   ();
    void createCodeEditor();
    void createMessages  ();
    void dismantler();
    std::string getTextFromForm(QTextEdit* form);
public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    bool slotSave      ();
    bool slotSaveAs    ();
    void slotConfEdit  ();
    void slotNew       ();
    void slotManualEdit();
    bool slotWarning   ();
    void slotAbout     ()
    {  //TODO  normal text for about form
        QMessageBox::about(this, "About \"Retracking\"", "SDI Example");
    }
    void on_pushButton_clicked();

    void on_ClearMessages_clicked();

    void on_logfile_check_stateChanged(int arg1);

    void on_checkBox_stateChanged(int arg1);

public slots:
    void debug_info    ();
    void slotLoad      ();
    void slotNoImpl    ()
    {
        QMessageBox::information(nullptr, "Message", "Not implemented");
    }
    void slotChangeWindowTitle(const QString& str)
    {
        setWindowTitle(str);
    }
signals:
    void changeWindowTitle(const QString&);
private:
    Ui::MainWindow *ui;
    void showEvent(QShowEvent *);
};

#endif // MAINWINDOW_H
