#include "MainWindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Bug_reports &report = Bug_reports::instance();
    MainWindow w;
    w.showMaximized();
    return a.exec();
}
