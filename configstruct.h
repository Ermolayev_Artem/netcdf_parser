#ifndef CONFIGSTRUCT_H
#define CONFIGSTRUCT_H

#include <iostream>
#include <cstdlib>
#include <string>
#include <map>
#include <algorithm>
#include <utility>

#include "ReportCodes.h"

#define DF_CONFIG_FN "retracking.conf"

struct struct_conf_f
{
    struct_conf_f(){}
public:
    int StartCycle    = 0;
    int EndCycle      = 0;
    long Lat1         = 0;
    long Lat2         = 0;
    long Lon1         = 0;
    long Lon2         = 0;
    uint64_t StartDat = 0;
    uint64_t EndDat   = 0;
    std::multimap<ATTR_CODE /*parameter*/, std::string /*value*/> list_config;
    std::string name_conf_nc;
    std::string name_conf;
    std::fstream ifs;
};
#endif // CONFIGSTRUCT_H
